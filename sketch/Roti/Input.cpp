#include <stddef.h>
#include <stdint.h>

#include "include/Input.h"

#include "ArduinoAdapter.h"
#include "Roti.h"

Input * Input::input = NULL;

Input * Input::Instance(){
    if(Input::input == NULL){
        Input::input = new Input();
    }
        
    return input;
}

Input::Input(){
    this->arduinoAdapter = NULL;
    this->roti = NULL;
}

Input::~Input(){

}

void Input::ValueMinusTwoPressed(){
    if(Instance()->GuardRotiObject()){
        Instance()->roti->RatedMinusTwo();
    }
}

void Input::ValueMinusOnePressed(){
    if(Instance()->GuardRotiObject()){
        Instance()->roti->RatedMinusOne();
    }
}

void Input::ValueZeroPressed(){
    if(Instance()->GuardRotiObject()){
        Instance()->roti->RatedZero();
    }
}

void Input::ValueOnePressed(){
    if(Instance()->GuardRotiObject()){
        Instance()->roti->RatedOne();
    }
}

void Input::ValueTwoPressed(){
    if(Instance()->GuardRotiObject()){
        Instance()->roti->RatedTwo();
    }
}

 void Input::InitializeButtons(){
    if(this->GuardArduinoAdapter()){
        this->arduinoAdapter->AttachInterrupt(BUTTON_MINUS_TWO, Input::ValueMinusTwoPressed, RISING);
        this->arduinoAdapter->AttachInterrupt(BUTTON_MINUS_ONE, Input::ValueMinusOnePressed, RISING);
        this->arduinoAdapter->AttachInterrupt(BUTTON_ZERO, Input::ValueZeroPressed, RISING);
        this->arduinoAdapter->AttachInterrupt(BUTTON_ONE, Input::ValueOnePressed, RISING);
        this->arduinoAdapter->AttachInterrupt(BUTTON_TWO, Input::ValueTwoPressed, RISING);
    }
}

void Input::SetArduinoAdapter(ArduinoAdapter *adapter){
    this->arduinoAdapter = adapter;
}

void Input::SetRoti(Roti *roti){
    this->roti = roti;
}

bool Input::GuardArduinoAdapter(){
    if(this->arduinoAdapter == NULL){
        throw ArduinoAdapterNotSetException();
    }
    return true;
}

bool Input::GuardRotiObject(){
    if(this->roti == NULL){
        throw RotiObjectNotSetException();
    }
    return true;
}
