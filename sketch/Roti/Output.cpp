#include <stddef.h>

#ifdef ARDUINO
    #include "LedControl.h"
#else
    #include "include/LedControlDummy.h"
#endif

#include "include/Output.h"
#include "include/Roti.h"

Output::Output(){
    this->roti = NULL;
    this->ledControl = NULL;
}

Output::~Output(){

}

void Output::SetLedControlObject(LedControl* ledControl){
    this->ledControl = ledControl;
}

void Output::SetRoti(Roti* roti){
    this->roti = roti;
}

void Output::InitializeDisplays(){
    if(GuardLedControl()){
        for(int number = INFO_MODULE; number < MODULE_COUNT; number++) {
            this->ledControl->shutdown(number, false);
            this->ledControl->setIntensity(number, DISPLAY_INTENSITY);
            this->ledControl->clearDisplay(number); 
        }
    }
} 

void Output::UpdateAverageDisplay(){
    if(GuardRequiredMembers()){
        float currentAverage = this->roti->CurrentAverage();
        this->ledControl->clearDisplay(INFO_MODULE);
        this->ledControl->setDigit(INFO_MODULE, 1, 2, true);
        this->ledControl->setDigit(INFO_MODULE, 2, 0, false);
        this->ledControl->setDigit(INFO_MODULE, 3, 0, false);
    }
}

void Output::UpdateMinusTwoDisplay(){
    if(GuardRequiredMembers()){
        char value = this->roti->CountRatedMinusTwo();
        this->UpdateRatingDisplay(MODULE_MINUS_TWO, value);
    }
}

void Output::UpdateMinusOneDisplay(){
    if(GuardRequiredMembers()){
        char value = this->roti->CountRatedMinusOne();
        this->UpdateRatingDisplay(MODULE_MINUS_ONE, value);
    }
}

void Output::UpdateZeroDisplay(){
    if(GuardRequiredMembers()){
        char value = this->roti->CountRatedZero();
        this->UpdateRatingDisplay(MODULE_ZERO, value);
    }
}

void Output::UpdateOneDisplay(){
    if(GuardRequiredMembers()){
        char value = this->roti->CountRatedOne();
        this->UpdateRatingDisplay(MODULE_ONE, value);
    }
}

void Output::UpdateTwoDisplay(){
    if(GuardRequiredMembers()){
        char value = this->roti->CountRatedTwo();
        this->UpdateRatingDisplay(MODULE_TWO, value);
    }
}

void Output::UpdateRatingDisplay(int moduleIndex, char value){
        this->ledControl->clearDisplay(moduleIndex);
        char tenSpot = value / 10;
        if(tenSpot > 0) {
            this->ledControl->setDigit(moduleIndex, SEVEN_SEGMENT_TENSPOT, tenSpot, false);
        }
        this->ledControl->setDigit(moduleIndex, SEVEN_SEGMENT_ONESPOT, value % 10, false);
}

void Output::ShowStartupMessage(){
    if(GuardLedControl()){
        this->ledControl->clearDisplay(INFO_MODULE);
        int L = 0b00001110, O = 0b01111110, S = 0b01011011;
        this->ledControl->setRow(INFO_MODULE, 1, L); 
        this->ledControl->setRow(INFO_MODULE, 2, O); 
        this->ledControl->setRow(INFO_MODULE, 3, S); 
    }
}

void Output::ResetFlash(){
    if(GuardLedControl()){
        ClearDisplays();   
        int Dash = 0b00000001;
        for(int number = 0; number < 4; number++) {
            this->ledControl->setRow(INFO_MODULE, number, Dash);
        } 
    }
}

void Output::ClearDisplays(){
    if(GuardLedControl()){
        for(int number = INFO_MODULE; number < MODULE_COUNT; number++) {
            this->ledControl->clearDisplay(number);
        } 
    }
}

bool Output::GuardRequiredMembers(){
    return this->GuardRoti () &&
            this->GuardLedControl();
}

bool Output::GuardLedControl(){
    if(this->ledControl == NULL)
        throw LedControlObjectNotSetException();
    return true;
}

bool Output::GuardRoti(){
    if(this->roti == NULL)
        throw RotiObjectNotSetException();
    return true;
}