#include "include/Roti.h"

Roti::Roti(){
}

Roti::~Roti(){

}

void Roti::Initialize(){
    this->ratedMinusTwo = 0;
    this->ratedMinusOne = 0;
    this->ratedZero = 0;
    this->ratedOne = 0;
    this->ratedTwo = 0;

}

float Roti::CurrentAverage(){
    if(CurrentRatingsSubmitted() != 0){
        return CurrentTotalRating() / CurrentRatingsSubmitted();
    }

    return (float)0.0;
}

float Roti::CurrentTotalRating(){
    return  (this->ratedMinusTwo * -2) +
            (this->ratedMinusOne * -1) +
            this->ratedOne +
            (this->ratedTwo * 2);
}

short Roti::CurrentRatingsSubmitted(){
    return this->ratedMinusTwo+
            this->ratedMinusOne +
            this->ratedZero +
            this->ratedOne +
            this->ratedTwo;
}

void Roti::RatedMinusTwo(){
    this->ratedMinusTwo ++;
}

void Roti::RatedMinusOne(){
    this->ratedMinusOne ++;
}

void Roti::RatedZero(){
    this->ratedZero ++;
}

void Roti::RatedOne(){
    this->ratedOne ++;
}

void Roti::RatedTwo(){
    this->ratedTwo ++;
}

char Roti::CountRatedMinusTwo(){
    return this->ratedMinusTwo;    
}

char Roti::CountRatedMinusOne(){
    return this->ratedMinusOne;    
}

char Roti::CountRatedZero(){
    return this->ratedZero;    
}

char Roti::CountRatedOne(){
    return this->ratedOne;
}

char Roti::CountRatedTwo(){
    return this->ratedTwo;
}
