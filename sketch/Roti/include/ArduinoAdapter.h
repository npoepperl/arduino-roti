#pragma once

typedef void (* InterruptCallback) ();


class ArduinoAdapter{

public:
    virtual void AttachInterrupt(uint8_t, InterruptCallback, int mode){}
};