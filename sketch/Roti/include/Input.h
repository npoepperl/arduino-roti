// declaring required classes
class ArduinoAdapter;
class Roti;

#include <exception>

class ArduinoAdapterNotSetException : public std::exception{
    const char* what(){
        return "The adapter to the Arduino SDK has not been set.";
    }
};

class RotiObjectNotSetException : public std::exception{
    const char* what(){
        return "The R.O.T.I. object has not been set.";
    }
};

// defines copied from Arduino.h
#ifndef RISING
#define RISING 3
#endif

// defines for pins to attach interrupts
#define BUTTON_MINUS_TWO    (uint8_t)1
#define BUTTON_MINUS_ONE    (uint8_t)2
#define BUTTON_ZERO         (uint8_t)3
#define BUTTON_ONE          (uint8_t)4
#define BUTTON_TWO          (uint8_t)5

class Input{
private:    
    static Input *input;
    Input();
    ~Input();

public:
    static Input * Instance();

    static void ValueMinusTwoPressed();
    static void ValueMinusOnePressed();
    static void ValueZeroPressed();
    static void ValueOnePressed();
    static void ValueTwoPressed();

    void InitializeButtons();

    void SetArduinoAdapter(ArduinoAdapter *adapter);
    void SetRoti(Roti *roti);

private:
    bool GuardArduinoAdapter();
    bool GuardRotiObject();
    
    ArduinoAdapter *arduinoAdapter;
    Roti *roti;
};