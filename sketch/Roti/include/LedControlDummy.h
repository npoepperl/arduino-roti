
class LedControl {

public:
    virtual void shutdown(int addr, bool status){}
    virtual void setIntensity(int addr, int intensity){}
    virtual void clearDisplay(int addr){}
    virtual void setDigit(int addr, int digit, char value, bool dp){}
    virtual void setRow(int addr, int digit, int value){}
};