#include <exception>

class LedControl;
class Roti;

#define MODULE_COUNT 6
#define MODULE_MINUS_TWO 5
#define MODULE_MINUS_ONE 4
#define MODULE_ZERO 3
#define MODULE_ONE 2
#define MODULE_TWO 1
#define INFO_MODULE 0
#define SEVEN_SEGMENT_TENSPOT 0
#define SEVEN_SEGMENT_ONESPOT 1
#define DISPLAY_INTENSITY 8

// Magic numbers concerning 7 segment displays
//                --
//               |  |
//                --
//               |  |
//                --
//
//  0b  0  0  0  0  0  0  0  0


class RotiObjectNotSetException : public std::exception{
    const char* what(){
        return "The ROTI object has not been set.";
    }
};

class LedControlObjectNotSetException : public std::exception{
    const char* what(){
        return "The LedControl object has not been set.";
    }
};

class Output {
    private:
        LedControl* ledControl;
        Roti* roti;
        bool GuardRequiredMembers();
        bool GuardLedControl();
        bool GuardRoti();

        void UpdateRatingDisplay(int moduleIndex, char value);

    public:
        Output();
        virtual ~Output();

        void SetLedControlObject(LedControl* ledControl);
        void InitializeDisplays(); 
        void SetRoti(Roti* roti);
        void UpdateAverageDisplay();
        void UpdateMinusTwoDisplay();
        void UpdateMinusOneDisplay();
        void UpdateZeroDisplay();
        void UpdateOneDisplay();
        void UpdateTwoDisplay();
        void ShowStartupMessage();
        void ClearDisplays();
        void ResetFlash();
};