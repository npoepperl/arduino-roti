#pragma once

class Roti{
public:
    Roti();
    ~Roti();

    void Initialize();

    virtual float CurrentAverage();

    virtual void RatedMinusTwo();
    virtual void RatedMinusOne();
    virtual void RatedZero();
    virtual void RatedOne();
    virtual void RatedTwo();

    virtual char CountRatedMinusTwo();
    virtual char CountRatedMinusOne();
    virtual char CountRatedZero();
    virtual char CountRatedOne();
    virtual char CountRatedTwo();

private:

    char ratedMinusTwo;
    char ratedMinusOne;
    char ratedZero;
    char ratedOne;
    char ratedTwo;

    float CurrentTotalRating();
    short CurrentRatingsSubmitted();
};

