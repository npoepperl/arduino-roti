#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Input.h"
#include "mocks/ArduinoAdapterMock.h"
#include "mocks/RotiMock.h"

class InputTests : public ::testing::Test {

protected:
    InputTests(){
    }

    virtual ~InputTests(){
    }

public:
};

TEST_F(InputTests, InitializingInputWithoutSettingArduinoAdapterResultsInException){
    Input* input = Input::Instance();
    ASSERT_THROW(input->InitializeButtons(), ArduinoAdapterNotSetException);
}

TEST_F(InputTests, InitializingInputWithInterruptValuesShouldNotFail){
    ArduinoAdapterMock mock;
    EXPECT_CALL(mock, AttachInterrupt(BUTTON_MINUS_TWO, Input::ValueMinusTwoPressed, RISING));
    EXPECT_CALL(mock, AttachInterrupt(BUTTON_MINUS_ONE, Input::ValueMinusOnePressed, RISING));
    EXPECT_CALL(mock, AttachInterrupt(BUTTON_ZERO, Input::ValueZeroPressed, RISING));
    EXPECT_CALL(mock, AttachInterrupt(BUTTON_ONE, Input::ValueOnePressed, RISING));
    EXPECT_CALL(mock, AttachInterrupt(BUTTON_TWO, Input::ValueTwoPressed, RISING));
    Input* input = Input::Instance();
    input->SetArduinoAdapter(&mock);
    input->InitializeButtons();
}

TEST_F(InputTests, PushingButtonMinusTwoWithoutSettingRotiResultsInException){
    ASSERT_THROW(Input::ValueMinusTwoPressed(), RotiObjectNotSetException);
}

TEST_F(InputTests, PushingButtonMinusOneWithoutSettingRotiResultsInException){
    ASSERT_THROW(Input::ValueMinusOnePressed(), RotiObjectNotSetException);
}

TEST_F(InputTests, PushingButtonZeroWithoutSettingRotiResultsInException){
    ASSERT_THROW(Input::ValueZeroPressed(), RotiObjectNotSetException);
}

TEST_F(InputTests, PushingButtonOneWithoutSettingRotiResultsInException){
    ASSERT_THROW(Input::ValueOnePressed(), RotiObjectNotSetException);
}

TEST_F(InputTests, PushingButtonTwoWithoutSettingRotiResultsInException){
    ASSERT_THROW(Input::ValueTwoPressed(), RotiObjectNotSetException);
}

TEST_F(InputTests, PushingButtonMinusTwoExpectsRatedMinusTwoToBeCalled){
    RotiMock mock;
    EXPECT_CALL(mock, RatedMinusTwo());
    Input* input = Input::Instance();
    input->SetRoti(&mock);
    Input::ValueMinusTwoPressed();
}

TEST_F(InputTests, PushingButtonMinusOneExpectsRatedMinusOneToBeCalled){
    RotiMock mock;
    EXPECT_CALL(mock, RatedMinusOne());
    Input* input = Input::Instance();
    input->SetRoti(&mock);
    Input::ValueMinusOnePressed();
}

TEST_F(InputTests, PushingButtonZeroExpectsRatedZeroToBeCalled){
    RotiMock mock;
    EXPECT_CALL(mock, RatedZero());
    Input* input = Input::Instance();
    input->SetRoti(&mock);
    Input::ValueZeroPressed();
}

TEST_F(InputTests, PushingButtonOneExpectsRatedOneToBeCalled){
    RotiMock mock;
    EXPECT_CALL(mock, RatedOne());
    Input* input = Input::Instance();
    input->SetRoti(&mock);
    Input::ValueOnePressed();
}

TEST_F(InputTests, PushingButtonTwoExpectsRatedTwoToBeCalled){
    RotiMock mock;
    EXPECT_CALL(mock, RatedTwo());
    Input* input = Input::Instance();
    input->SetRoti(&mock);
    Input::ValueTwoPressed();
}