#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Output.h"
#include "mocks/LedControlMock.h"
#include "mocks/RotiMock.h"

using ::testing::Return;
using ::testing::NiceMock;

class OutputTests : public ::testing::Test {

protected:
    OutputTests (){

    }

    ~OutputTests () {

    }

};

TEST_F(OutputTests, InititializingDisplaysShouldNotFail){
    LedControlMock mock;
    EXPECT_CALL(mock, shutdown(0, false));
    EXPECT_CALL(mock, shutdown(1, false));
    EXPECT_CALL(mock, shutdown(2, false));
    EXPECT_CALL(mock, shutdown(3, false));
    EXPECT_CALL(mock, shutdown(4, false));
    EXPECT_CALL(mock, shutdown(5, false));
    EXPECT_CALL(mock, setIntensity(0, 8));
    EXPECT_CALL(mock, setIntensity(1, 8));
    EXPECT_CALL(mock, setIntensity(2, 8));
    EXPECT_CALL(mock, setIntensity(3, 8));
    EXPECT_CALL(mock, setIntensity(4, 8));
    EXPECT_CALL(mock, setIntensity(5, 8));
    EXPECT_CALL(mock, clearDisplay(0));
    EXPECT_CALL(mock, clearDisplay(1));
    EXPECT_CALL(mock, clearDisplay(2));
    EXPECT_CALL(mock, clearDisplay(3));
    EXPECT_CALL(mock, clearDisplay(4));
    EXPECT_CALL(mock, clearDisplay(5));

    Output output;
    output.SetLedControlObject(&mock);
    output.InitializeDisplays();
}

TEST_F(OutputTests, DisplayInitializationWithoutLedControlShouldThrowException){
    Output output;
    ASSERT_THROW(output.InitializeDisplays(), LedControlObjectNotSetException);
}

TEST_F(OutputTests, UpdateAverageDisplayWithoutRotiShouldThrowException){
    Output output;
    ASSERT_THROW(output.UpdateAverageDisplay(), RotiObjectNotSetException);
}

TEST_F(OutputTests, UpdateAverageDisplayWithoutLedControlShouldThrowException){
    Output output;
    RotiMock rotiMock;
    output.SetRoti(&rotiMock);
    ASSERT_THROW(output.UpdateAverageDisplay(), LedControlObjectNotSetException);
}

TEST_F(OutputTests, UpdateDisplayShouldQueryCurrentAverageFromRoti){
    RotiMock rotiMock;
    LedControlMock ledControlMock;
    EXPECT_CALL(rotiMock, CurrentAverage()).WillOnce(Return(42.00f));
    Output output;
    output.SetRoti(&rotiMock);
    output.SetLedControlObject(&ledControlMock);
    output.UpdateAverageDisplay();
}

TEST_F(OutputTests, UpdateAverageDisplayShouldSendExpectedValueToLedControl){
    RotiMock rotiMock;
    LedControlMock ledControlMock;

    ON_CALL(rotiMock, CurrentAverage()).WillByDefault(Return(2.0f));

    EXPECT_CALL(ledControlMock, clearDisplay(INFO_MODULE));
    EXPECT_CALL(ledControlMock, setDigit(INFO_MODULE, 1, 2, true));
    EXPECT_CALL(ledControlMock, setDigit(INFO_MODULE, 2, 0, false));
    EXPECT_CALL(ledControlMock, setDigit(INFO_MODULE, 3, 0, false));

    Output output;
    output.SetRoti(&rotiMock);
    output.SetLedControlObject(&ledControlMock);
    output.UpdateAverageDisplay();
}

TEST_F(OutputTests, UpdateMinusTwoDisplayShouldSetValueToMinusTwoDisplay){
    RotiMock  rotiMock;
    LedControlMock ledControlMock;

    ON_CALL(rotiMock, CountRatedMinusTwo()).WillByDefault(Return(5));

    EXPECT_CALL(ledControlMock, clearDisplay(MODULE_MINUS_TWO));
    EXPECT_CALL(ledControlMock, setDigit(MODULE_MINUS_TWO, SEVEN_SEGMENT_ONESPOT, 5, false));

    Output output;
    output.SetRoti(&rotiMock);
    output.SetLedControlObject(&ledControlMock);
    output.UpdateMinusTwoDisplay();
}

TEST_F(OutputTests, UpdateMinusTwoDisplayShouldSetValueGreater10ToMinusTwoDisplay){
    RotiMock  rotiMock;
    LedControlMock ledControlMock;

    ON_CALL(rotiMock, CountRatedMinusTwo()).WillByDefault(Return(14));

    EXPECT_CALL(ledControlMock, clearDisplay(MODULE_MINUS_TWO));
    EXPECT_CALL(ledControlMock, setDigit(MODULE_MINUS_TWO, SEVEN_SEGMENT_TENSPOT, 1, false));
    EXPECT_CALL(ledControlMock, setDigit(MODULE_MINUS_TWO, SEVEN_SEGMENT_ONESPOT, 4, false));

    Output output;
    output.SetRoti(&rotiMock);
    output.SetLedControlObject(&ledControlMock);
    output.UpdateMinusTwoDisplay();
}

TEST_F(OutputTests, UpdateMinusOneDisplayShouldSetValueGreater10ToMinusOneDisplay){
    RotiMock  rotiMock;
    LedControlMock ledControlMock;

    ON_CALL(rotiMock, CountRatedMinusOne()).WillByDefault(Return(12));

    EXPECT_CALL(ledControlMock, clearDisplay(MODULE_MINUS_ONE));
    EXPECT_CALL(ledControlMock, setDigit(MODULE_MINUS_ONE, SEVEN_SEGMENT_TENSPOT, 1, false));
    EXPECT_CALL(ledControlMock, setDigit(MODULE_MINUS_ONE, SEVEN_SEGMENT_ONESPOT, 2, false));

    Output output;
    output.SetRoti(&rotiMock);
    output.SetLedControlObject(&ledControlMock);
    output.UpdateMinusOneDisplay();
}

TEST_F(OutputTests, UpdateZeroDisplayShouldSetValueToZeroDisplay){
    RotiMock  rotiMock;
    LedControlMock ledControlMock;

    ON_CALL(rotiMock, CountRatedZero()).WillByDefault(Return(8));

    EXPECT_CALL(ledControlMock, clearDisplay(MODULE_ZERO));
    EXPECT_CALL(ledControlMock, setDigit(MODULE_ZERO, SEVEN_SEGMENT_ONESPOT, 8, false));

    Output output;
    output.SetRoti(&rotiMock);
    output.SetLedControlObject(&ledControlMock);
    output.UpdateZeroDisplay();

}

TEST_F(OutputTests, UpdateOneDisplayShouldSetValueToOneDisplay){
    RotiMock  rotiMock;
    LedControlMock ledControlMock;

    ON_CALL(rotiMock, CountRatedOne()).WillByDefault(Return(97));

    EXPECT_CALL(ledControlMock, clearDisplay(MODULE_ONE));
    EXPECT_CALL(ledControlMock, setDigit(MODULE_ONE, SEVEN_SEGMENT_TENSPOT, 9, false));
    EXPECT_CALL(ledControlMock, setDigit(MODULE_ONE, SEVEN_SEGMENT_ONESPOT, 7, false));

    Output output;
    output.SetRoti(&rotiMock);
    output.SetLedControlObject(&ledControlMock);
    output.UpdateOneDisplay();

}

TEST_F(OutputTests, UpdateTwoDisplayShouldSetValueToTwoDisplay){
    RotiMock  rotiMock;
    LedControlMock ledControlMock;

    ON_CALL(rotiMock, CountRatedTwo()).WillByDefault(Return(99));

    EXPECT_CALL(ledControlMock, clearDisplay(MODULE_TWO));
    EXPECT_CALL(ledControlMock, setDigit(MODULE_TWO, SEVEN_SEGMENT_TENSPOT, 9, false));
    EXPECT_CALL(ledControlMock, setDigit(MODULE_TWO, SEVEN_SEGMENT_ONESPOT, 9, false));

    Output output;
    output.SetRoti(&rotiMock);
    output.SetLedControlObject(&ledControlMock);
    output.UpdateTwoDisplay();

}

TEST_F(OutputTests, ShowStartupMessageWithoutLedControlShouldThrowException){
    Output output;
    ASSERT_THROW(output.ShowStartupMessage(), LedControlObjectNotSetException);
}

TEST_F(OutputTests, ShowStartupMessageShouldSendTheRightCharsToLedControl){
    LedControlMock ledControlMock;

    EXPECT_CALL(ledControlMock, clearDisplay(INFO_MODULE));
    EXPECT_CALL(ledControlMock, setRow(INFO_MODULE, 1, 0b00001110));
    EXPECT_CALL(ledControlMock, setRow(INFO_MODULE, 2, 0b01111110));
    EXPECT_CALL(ledControlMock, setRow(INFO_MODULE, 3, 0b01011011));

    Output output;
    output.SetLedControlObject(&ledControlMock);
    output.ShowStartupMessage();
}

TEST_F(OutputTests, ClearDisplaysWithoutLedControlShouldThrowException){
    Output output;
    ASSERT_THROW(output.ClearDisplays(), LedControlObjectNotSetException);
}

TEST_F(OutputTests, ClearDisplaysShouldWipeTheDisplaysViaLedControlClearDisplay){
    LedControlMock ledControlMock;

    EXPECT_CALL(ledControlMock, clearDisplay(INFO_MODULE));
    EXPECT_CALL(ledControlMock, clearDisplay(1));
    EXPECT_CALL(ledControlMock, clearDisplay(2));
    EXPECT_CALL(ledControlMock, clearDisplay(3));
    EXPECT_CALL(ledControlMock, clearDisplay(4));
    EXPECT_CALL(ledControlMock, clearDisplay(5));

    Output output;
    output.SetLedControlObject(&ledControlMock);
    output.ClearDisplays();
}

TEST_F(OutputTests, ResetFlashWithoutLedControlShoudThrowException){
    Output output;
    ASSERT_THROW(output.ResetFlash(), LedControlObjectNotSetException);
}

TEST_F(OutputTests, ResetFlashShouldClearDisplayViaLedControl){
    NiceMock<LedControlMock> ledControlMock;

    EXPECT_CALL(ledControlMock, clearDisplay(INFO_MODULE));
    EXPECT_CALL(ledControlMock, clearDisplay(1));
    EXPECT_CALL(ledControlMock, clearDisplay(2));
    EXPECT_CALL(ledControlMock, clearDisplay(3));
    EXPECT_CALL(ledControlMock, clearDisplay(4));
    EXPECT_CALL(ledControlMock, clearDisplay(5));

    Output output;
    output.SetLedControlObject(&ledControlMock);
    output.ResetFlash();
}

TEST_F(OutputTests, ResetFlashShouldSendDashesToLedControlInfoModule){
    LedControlMock ledControlMock;

    EXPECT_CALL(ledControlMock, setRow(INFO_MODULE, 0, 0b00000001));
    EXPECT_CALL(ledControlMock, setRow(INFO_MODULE, 1, 0b00000001));
    EXPECT_CALL(ledControlMock, setRow(INFO_MODULE, 2, 0b00000001));
    EXPECT_CALL(ledControlMock, setRow(INFO_MODULE, 3, 0b00000001));

    Output output;
    output.SetLedControlObject(&ledControlMock);
    output.ResetFlash();
}
