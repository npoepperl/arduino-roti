#include "gtest/gtest.h"
#include "Roti.h"

class RotiTests : public ::testing::Test {

protected:
    RotiTests(){
        roti = new Roti();
    }

    virtual ~RotiTests(){
        delete roti;
    }

public:
    Roti* roti;
};

TEST_F(RotiTests, InitializingRotiResultsInZeroAverage){
    this->roti->Initialize();
    ASSERT_EQ((float)0.0, roti->CurrentAverage());
}

TEST_F(RotiTests, InitializingRotiResultsInZeroOnMinusTwo){
    this->roti->Initialize();
    ASSERT_EQ((char)0, roti->CountRatedMinusTwo());
}

TEST_F(RotiTests, InitializingRotiResultsInZeroOnMinusOne){
    this->roti->Initialize();
    ASSERT_EQ((char)0, roti->CountRatedMinusOne());
}

TEST_F(RotiTests, InitializingRotiResultsInZeroOnZero){
    this->roti->Initialize();
    ASSERT_EQ((char)0, roti->CountRatedZero());
}

TEST_F(RotiTests, InitializingRotiResultsInZeroOnOne){
    this->roti->Initialize();
    ASSERT_EQ((char)0, roti->CountRatedOne());
}

TEST_F(RotiTests, InitializingRotiResultsInZeroOnTwo){
    this->roti->Initialize();
    ASSERT_EQ((char)0, roti->CountRatedTwo());
}

TEST_F(RotiTests, NewRotiAfterPushingRatingMinusTwoResultsInCountRatedMinusTwoEquals1){
    this->roti->Initialize();
    this->roti->RatedMinusTwo();
    ASSERT_EQ(1, roti->CountRatedMinusTwo());
}

TEST_F(RotiTests, NewRotiAfterPushingRatingMinusOneResultsInCountRatedMinusOneEquals1){
    this->roti->Initialize();
    this->roti->RatedMinusOne();
    ASSERT_EQ(1, roti->CountRatedMinusOne());
}

TEST_F(RotiTests, NewRotiAfterPushingRatingMinusOneTwiceResultsInCountRatedMinusOneEquals2){
    this->roti->Initialize();
    this->roti->RatedMinusOne();
    this->roti->RatedMinusOne();
    ASSERT_EQ(2, roti->CountRatedMinusOne());
}

TEST_F(RotiTests, NewRotiAfterPushingRatingZeroResultsInCountRatedZeroEquals1){
    this->roti->Initialize();
    this->roti->RatedZero();
    ASSERT_EQ(1, roti->CountRatedZero());
}

TEST_F(RotiTests, NewRotiAfterPushingRatingOneResultsInCountRatedOneEquals1){
    this->roti->Initialize();
    this->roti->RatedOne();
    ASSERT_EQ(1, roti->CountRatedOne());
}

TEST_F(RotiTests, NewRotiAfterPushingRatingOneTwiceResultsInCountRatedOneEquals2){
    this->roti->Initialize();
    this->roti->RatedOne();
    this->roti->RatedOne();
    ASSERT_EQ(2, roti->CountRatedOne());
}

TEST_F(RotiTests, NewRotiAfterPushingRatingTwoResultsInCountRatedTwoEquals1){
    this->roti->Initialize();
    this->roti->RatedTwo();
    ASSERT_EQ(1, roti->CountRatedTwo());
}

TEST_F(RotiTests, NewRotiAfterPushingRatingTwoTwiceResultsInCountRatedTwoEquals2){
    this->roti->Initialize();
    this->roti->RatedTwo();
    this->roti->RatedTwo();
    ASSERT_EQ(2, roti->CountRatedTwo());
}

TEST_F(RotiTests, NewRotiAfterPushingRatingMinusTwoTwiceResultsInAverageMinus2){
    this->roti->Initialize();
    this->roti->RatedMinusTwo();
    this->roti->RatedMinusTwo();
    ASSERT_EQ(-2, roti->CurrentAverage());
}

TEST_F(RotiTests, NewRotiAfterPushingRatingMinusOneTwiceResultsInAverageMinus1){
    this->roti->Initialize();
    this->roti->RatedMinusOne();
    this->roti->RatedMinusOne();
    ASSERT_EQ(-1, roti->CurrentAverage());
}

TEST_F(RotiTests, NewRotiAfterPushingRatingOneTwiceResultsInAverage1){
    this->roti->Initialize();
    this->roti->RatedOne();
    this->roti->RatedOne();
    ASSERT_EQ(1, roti->CurrentAverage());
}

TEST_F(RotiTests, NewRotiAfterPushingRatingTwoTwiceResultsInAverage2){
    this->roti->Initialize();
    this->roti->RatedTwo();
    this->roti->RatedTwo();
    ASSERT_EQ(2, roti->CurrentAverage());
}

TEST_F(RotiTests, NewRotiAfterPushingAllButtonsOnceExpectsCountsOfOneAndAverageOfZero){
    this->roti->Initialize();
    this->roti->RatedMinusTwo();
    this->roti->RatedMinusOne();
    this->roti->RatedZero();
    this->roti->RatedOne();
    this->roti->RatedTwo();
    EXPECT_EQ(1, roti->CountRatedMinusTwo());
    EXPECT_EQ(1, roti->CountRatedMinusOne());
    EXPECT_EQ(1, roti->CountRatedZero());
    EXPECT_EQ(1, roti->CountRatedOne());
    EXPECT_EQ(1, roti->CountRatedTwo());
    ASSERT_EQ(0, roti->CurrentAverage());
}
