#include "gmock/gmock.h"
#include "ArduinoAdapter.h"

class ArduinoAdapterMock : public ArduinoAdapter{

public:
    MOCK_METHOD3(AttachInterrupt, void(uint8_t, InterruptCallback, int mode));
};
