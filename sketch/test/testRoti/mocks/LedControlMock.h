#include "gmock/gmock.h"

#include "LedControlDummy.h"

class LedControlMock : public LedControl {

public:
    MOCK_METHOD2(shutdown, void(int, bool));
    MOCK_METHOD2(setIntensity,void(int, int));
    MOCK_METHOD1(clearDisplay, void(int));
    MOCK_METHOD4(setDigit, void(int, int, char, bool));
    MOCK_METHOD3(setRow, void(int, int, int));
};