#include "gmock/gmock.h"
#include "Roti.h"

class RotiMock : public Roti{
public:
    MOCK_METHOD0(RatedMinusTwo, void());  
    MOCK_METHOD0(RatedMinusOne, void());  
    MOCK_METHOD0(RatedZero, void());  
    MOCK_METHOD0(RatedOne, void());  
    MOCK_METHOD0(RatedTwo, void());  
    MOCK_METHOD0(CurrentAverage, float());
    MOCK_METHOD0(CountRatedMinusTwo, char());
    MOCK_METHOD0(CountRatedMinusOne, char());
    MOCK_METHOD0(CountRatedZero, char());
    MOCK_METHOD0(CountRatedOne, char());
    MOCK_METHOD0(CountRatedTwo, char());
};
